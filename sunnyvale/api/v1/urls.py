from rest_framework import routers

from .accounts import views as accounts_views
from .polls import views as polls_views

router = routers.DefaultRouter()

router.register(r'users', accounts_views.UserViewSet)
router.register(r'questions', polls_views.QuestionViewSet)
router.register(r'choices', polls_views.ChoiceViewSet)

urlpatterns = router.urls
