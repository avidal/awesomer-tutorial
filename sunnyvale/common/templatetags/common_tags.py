from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()


@register.filter
@stringfilter
def shout(text, shoutitude=4):
    """
    Makes text *really* stand up and shout!
    Add a single argument to change the amount of shoutitude, default is 4.
    """
    return text.upper() + '!' * shoutitude


@register.filter
@stringfilter
def excite(text):
    """
    Oh man I am *so* excited about this text!
    """
    return 'Oh man! Oh man! ' + text
