# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def forwards_func(apps, schema_editor):
    Choice = apps.get_model("polls", "Choice")
    db_alias = schema_editor.connection.alias

    Choice.objects.using(db_alias).all().update(vote_count=models.F('votes'))


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0004_auto_20141024_1319'),
    ]

    operations = [
        migrations.RunPython(forwards_func),
    ]
