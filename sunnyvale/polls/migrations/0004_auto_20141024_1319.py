# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0003_auto_20141024_1241'),
    ]

    operations = [
        migrations.AddField(
            model_name='choice',
            name='vote_count',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='question',
            name='vote_count',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
