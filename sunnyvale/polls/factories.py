import datetime

import factory
import factory.fuzzy

from django.utils import timezone

from accounts.factories import UserFactory

START = timezone.now() - datetime.timedelta(days=365)


class QuestionFactory(factory.django.DjangoModelFactory):

    user = factory.SubFactory(UserFactory)
    question_text = factory.fuzzy.FuzzyText()
    pub_date = factory.fuzzy.FuzzyDateTime(START)

    class Meta:
        model = 'polls.Question'


class ChoiceFactory(factory.django.DjangoModelFactory):

    question = factory.SubFactory(QuestionFactory)
    choice_text = factory.fuzzy.FuzzyText()

    class Meta:
        model = 'polls.Choice'
