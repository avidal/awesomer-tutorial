import random

from django.core.management.base import BaseCommand

from accounts.factories import UserFactory
from polls.factories import QuestionFactory, ChoiceFactory

USER_COUNT = 10
QUESTION_COUNT = 1000
MAX_CHOICES = 10


class Command(BaseCommand):
    help = "Generate a number of questions automatically."

    def handle(self, *args, **options):
        users = UserFactory.create_batch(USER_COUNT)

        for i in range(QUESTION_COUNT):
            user = random.choice(users)
            question = QuestionFactory(user=user)

            nchoices = random.randint(1, MAX_CHOICES)

            ChoiceFactory.create_batch(nchoices, question=question)
