from django.core.urlresolvers import reverse
from django.conf import settings
from django.shortcuts import redirect
from django.views import generic

from accounts.forms import RegistrationForm
from accounts.tasks import send_registration_email


class RegisterView(generic.FormView):
    form_class = RegistrationForm
    template_name = 'accounts/register.html'

    def form_valid(self, form):
        user = form.save(commit=False)
        user.set_password(form.cleaned_data['password1'])
        user.save()

        # Send them an email!

        # request.build_absolute_uri takes a path that doesn't contain a scheme, hostname, or port;
        # and uses that data from the request to construct a full uri
        login_url = self.request.build_absolute_uri(reverse('accounts:login'))

        send_registration_email(user.id, login_url)

        # Send them to the login page after creating a new account.
        return redirect(settings.LOGIN_URL)
