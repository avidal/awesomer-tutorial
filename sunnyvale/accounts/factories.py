import factory
import factory.fuzzy


class UserFactory(factory.django.DjangoModelFactory):

    username = factory.fuzzy.FuzzyText(length=5, prefix='user-')

    class Meta:
        model = 'auth.User'
