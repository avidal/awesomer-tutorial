from django.core.mail import send_mail
from django.conf import settings
from django.contrib.auth.models import User
from django.template.loader import render_to_string

from celery import shared_task


@shared_task
def send_registration_email(user_id, login_url):
    user = User.objects.get(pk=user_id)

    ctx = dict(user=user, login_url=login_url)
    body = render_to_string('accounts/email/registration_body.txt', ctx)

    to = [user.email]

    send_mail("Thank you for registering!", body, settings.DEFAULT_FROM_EMAIL, to)
