from django import forms
from django.contrib import auth
from django.contrib.auth.models import User

from crispy_forms.helper import FormHelper
from crispy_forms import layout, bootstrap


class RegistrationForm(forms.ModelForm):
    password1 = forms.CharField(widget=forms.PasswordInput,
                                label="Password")
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'required'}),
                                label="Password Again")

    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)
        self.fields['username'].help_text = 'Ali!!!!'

    class Meta:
        model = User
        fields = ('username', 'email')

    def clean_username(self):
        username = self.cleaned_data['username']

        exists = User.objects.filter(username=username).exists()
        if exists:
            raise forms.ValidationError("This username already exists.")
        return username

    def clean(self):
        data = self.cleaned_data
        if not data.get('password1') == data.get('password2'):
            raise forms.ValidationError("Passwords must match!")
        return data


class AuthenticationForm(auth.forms.AuthenticationForm):

    def __init__(self, *args, **kwargs):
        super(AuthenticationForm, self).__init__(*args, **kwargs)
        helper = FormHelper(self)

        helper.form_class = 'form-horizontal'
        helper.label_class = 'col-md-2'
        helper.field_class = 'col-md-8'

        helper.layout = layout.Layout(
            'username',
            'password',
            layout.Div(
                layout.Div(
                    bootstrap.StrictButton('Login', css_class='btn-primary', type='submit'),
                    css_class='col-md-offset-2 col-md-8'
                ),
                css_class='form-group'
            ),
        )

        self.helper = helper
